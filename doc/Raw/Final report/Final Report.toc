\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}Literature review}{4}
\contentsline {subsection}{\numberline {2.1}Git and Git servers}{4}
\contentsline {subsection}{\numberline {2.2}Search algorithm within different data structure}{4}
\contentsline {subsubsection}{\numberline {2.2.1}Tree}{5}
\contentsline {subsubsection}{\numberline {2.2.2}B-Tree}{6}
\contentsline {subsubsection}{\numberline {2.2.3}Trie}{7}
\contentsline {subsection}{\numberline {2.3}Conclusion}{8}
\contentsline {section}{\numberline {3}Achievements}{10}
\contentsline {subsection}{\numberline {3.1}Preliminary decision}{10}
\contentsline {subsubsection}{\numberline {3.1.1}Development process}{10}
\contentsline {subsubsection}{\numberline {3.1.2}Language and modules choices}{10}
\contentsline {subsection}{\numberline {3.2}Development fulfilment}{11}
\contentsline {subsubsection}{\numberline {3.2.1}Backend}{11}
\contentsline {paragraph}{\nonumberline Rooting -}{11}
\contentsline {paragraph}{\nonumberline Cookies -}{12}
\contentsline {subsubsection}{\numberline {3.2.2}Frontend}{12}
\contentsline {subsection}{\numberline {3.3}Data management and search engine}{13}
\contentsline {subsubsection}{\numberline {3.3.1}SQL}{13}
\contentsline {paragraph}{\nonumberline Schema -}{13}
\contentsline {paragraph}{\nonumberline Tools -}{15}
\contentsline {subsubsection}{\numberline {3.3.2}Trie}{15}
\contentsline {paragraph}{\nonumberline Tools -}{15}
\contentsline {subsubsection}{\numberline {3.3.3}Speed retrieval comparison}{15}
\contentsline {subsection}{\numberline {3.4}API Scraping}{17}
\contentsline {paragraph}{\nonumberline Information gathering -}{17}
\contentsline {paragraph}{\nonumberline JSON parsing -}{18}
\contentsline {section}{\numberline {4}Methodology}{19}
\contentsline {subsection}{\numberline {4.1}Research}{19}
\contentsline {subsection}{\numberline {4.2}Development}{20}
\contentsline {subsubsection}{\numberline {4.2.1}Git workflow}{22}
\contentsline {subsubsection}{\numberline {4.2.2}Cabal and software architecture}{22}
\contentsline {section}{\numberline {5}Risk management}{25}
\contentsline {subsection}{\numberline {5.1}Risk identification}{25}
\contentsline {subsection}{\numberline {5.2}Risk avoidance}{25}
\contentsline {section}{\numberline {6}Dissertation conclusion}{27}
\contentsline {subsection}{\numberline {6.1}Technical achievement}{27}
\contentsline {subsection}{\numberline {6.2}Discussion}{27}
\contentsline {subsection}{\numberline {6.3}Personal acquisition of knowledge and Conclusion}{28}
\contentsline {section}{\numberline {7}Critical appraisal}{29}
\contentsline {subsection}{\numberline {7.1}Legal, social, ethical and professional issues}{29}
\contentsline {subsection}{\numberline {7.2}Potential improvement}{29}
\contentsline {subsection}{\numberline {7.3}Acknowledgment}{30}
