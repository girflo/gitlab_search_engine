# An Haskell search engine for open source projects
**Oxford Brookes University**

Florian Girardo
Supervisor: Dr. Ian Bayley

---

## Objectives
* Create a search engine for open source projects **hosted on several Gitlab servers**
* Improve personal's Haskell skills

---

## Motivations
* Create an **alternative to Github**
* Improve Gitlab hosted projects' visibility
* Support open source ideology

---

## Alternatives
* alternativeto.net
* bitbucket.org
* launchpad.net

---

## Why Haskell ?
* **Functional programming** skills
* Lazy Evaluation
* **Safe multithreading**

--- 

## Why Gitlab ?
* Stable software
* Rich API
* Widely used
* Supported by big companies

---

## Features
* Provide a web interface
* Gather Gitlab's information
* Store the information
* Retrieve ordered information 
* Create an good user experience by doing it quickly

---

## Trie data structure
* Variant of a tree
* **Key - value** storing

![Image of a trie](images/trie.jpg)

--- 

## Database study
* Database kind comparison using the trie data structure
* SQL - MariaDB
* NoSQL (key-value) - Redis
* NoSQL (Haskell data structures) - Acid-state

---


## Advantages of a web application
* **Cross-plateform**
* Easy to set up
* Easy to update
* Micro-services

---

## Disadvantage of a web application
* Server security
* Hosting price
* Downtime risks
* Avoid single point of failure

---

## Haskell's modules
* **scotty** - RESTful web framework
* **blaze-html** - Templating
* **http-streams** - Http client
* **hedis / mysql-simple / acid-states** - Database connectors 

---

## Current state
* Gitlab servers management
* MariaDB and Redis compatibility
* Web interface backbone
* Retrieve information from Gitlab API 
---

## Remaining work
* Run the test for the database study
* Link all scripts between them
* Create a welcoming interface
* Build the research script 
* Authentication

---

## Ethical and legal issues
* Web crawling and **server overload**
* Data Protection Act 
* Need **server approval** provided by token

--- 

## Thanks for listening
* Haskell search engine
* Aim to improve open source projects' visibility
* Trie data structure
* Web interface using micro-services architecture