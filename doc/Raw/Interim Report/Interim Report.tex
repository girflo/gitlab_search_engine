\documentclass{scrartcl}
\usepackage{bold-extra}
\usepackage{graphicx}
\graphicspath{ {images/} }
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\chead{An Haskell search engine for open source projects}
\cfoot{\thepage}
\usepackage{listings}
\usepackage{xcolor}
\lstset{
  showspaces=false,
  showtabs=false,
  breaklines=true,
  showstringspaces=false,
  breakatwhitespace=true,
  escapeinside={(*@}{@*)},
  basicstyle=\ttfamily,
  columns=fullflexible,
  frame=single,
  language=Haskell
}


\begin{document}
\title{An Haskell search engine for open source projects - Interim report}
\subtitle{Oxford Brookes University - Supervisor: Dr. Ian Bayley}
\date{2017}
\author{Girardo Florian}
\maketitle

\newpage
\tableofcontents
\newpage

\section{Introduction and methodology}
This document cover the process of development of a search engine for open sourced project hosted on several Gitlab servers.
The search engine is done using the Haskell programming language and Haskell's modules.

As opposed to a classic monolithic approach where the entire program is done in one piece of software I have chosen to create this engine using the micro-services architecture.
This method imply to create each features of the program as a total stand alone script, leading to a software easier to maintain and might be more suitable for a large database \cite{SingletonAndy2016TEoM}.
For this software the services will be the following :
\begin{enumerate}
  \item Requesting Gitlab's API.
  \item Storing and retrieving information from database.
  \item Sorting information using a search algorithm.
\end{enumerate}

The main advantage of the micro-service architecture is to separate completely each script, if one of them stop working the rest of the application will still be functional.
However as a down side of the architecture the project can be a little bit heavier if for example two separated scripts need the same module it will be compiled twice.
The choice of this architecture has been done after reflection on several personal project done as monolith most of them became hard to maintain mainly because each time that the code needs to be edited the entire application needs to be understand perfectly to avoid any regression bug.

Again by experience I knew that the installation of Gitlab can be a time consuming process, therefore I choose this time to use a pre-configurated Docker container to install it on my computer as I needed a server only for testing the requesting API script.
Once Docker installed on a computer the following command needs to be executed to run Gitlab as explained in the official documentation \cite{docker} :
\begin{lstlisting}
  sudo docker run --detach \
    --hostname gitlab.example.com \
    --env GITLAB_OMNIBUS_CONFIG="external_url 'http://my.domain.com/'; gitlab_rails['lfs_enabled'] = true;" \
    --publish 443:443 --publish 80:80 --publish 22:22 \
    --name gitlab \
    --restart always \
    --volume /srv/gitlab/config:/etc/gitlab \
    --volume /srv/gitlab/logs:/var/log/gitlab \
    --volume /srv/gitlab/data:/var/opt/gitlab \
    gitlab/gitlab-ce:latest
\end{lstlisting}

Gitlab has been launched in 2011 and has now became a robust project with several features out of the box like a complete API able to supply any non-confidential information stored on the server.
Obviously an authentication is needed to access that kind of information, Gitlab support OAuth2 tokens, private tokens, personal access tokens \cite{api}.
For this project the best tokens are the personal access one as the server's administrator can manage which information can be accessed by the token.
This token offer two different way to get enough rights to perform a query, the first one is the sudo parameter which works like in a Linux distribution.
The second one are specific tokens called impersonation tokens which are created by an administrator for a specific user, this solution is more suitable for this software as it's not necessary to provide therefore to store any password.
This token can also be revoke at any time by the administrator.

The script that needs to communicate with this API is going to send HTTP or HTTPS requests (depending on the domain) with the following format :
\begin{lstlisting}
  domain"/api/v4/"location"?private_token="token
\end{lstlisting}

Where location is the needed information for example ``projects'' or ``users''.
There are several Haskell's modules that aim to perform HTTP requests, the embedded HTTP Library \cite{http} could be the most appropriate but don't support HTTPS connections which can leads to errors depending the configuration of the Gitlab servers.
The use of the HTTP-Streams module \cite{stream} is going to resolve this issue and help us querying any Gitlab server.

For this script our software should be able to store the following information in a database :
\begin{itemize}
  \item Domains : Each domain that need to be requested.
  \item Token : The token created for each domain.
  \item What information to request for each domain.
  \item How often the script needs to update the information.
\end{itemize}

For this kind of data the most convenient database will be a SQL one.

Regarding the databases storing and retrieving, as the final document will contain a study between different kind of databases for this specific case the script should be able to work with each chosen databases :
\begin{itemize}
  \item Redis : Using the hedis package \cite{hedis}.
  \item MariaDB : Using the mysql-haskell package \cite{sql}.
  \item Acid-Static : Using the acid-static package \cite{acid}.
\end{itemize}

The Trie data-structure is a way to store and retrieve data designed for facilitate strings research.
It will be used for the search engine as it fit this kind of application.
Therefore the study will focus on the storing and retrieval speed of the data and on the weight of the database.
The execution time will be determined with the help of the criterion package \cite{criterion}.

Following this study the final project will be implemented using the most efficient database between the three.

The final script is going to perform the research among the previously retrieved data. The data should be remove from the result if not relevant while the remaining must by displayed sorted by relevancy.
A user will be therefore able to search for a project by it's name, description, tags or user. The same user will also be able to research for a specific developer by it's name or username.
Knowing that the search engine will only take into account the following fields from the API:

\begin{itemize}
  \item Projects : description, tag\_list, owner, name \cite{projects}
  \item Users : username, name \cite{users}
\end{itemize}

As this software will be an online tool the user interface will be created using a Haskell's web framework.
In order to observe the will expressed above to implement a micro-services based application the framework should support this architecture.
Among the several framework capable of using the architecture Scotty \cite{scotty} seems to be the most appropriate for this application as each of the features needed such as routing can be provided by it.
The use of a bigger framework such a Yesod can leads to a longer learning process that can be less meaningful with micro-services.

The web interface will provide a route for each of this feature :
\begin{itemize}
  \item Add Gitlab server's information.
  \item Edit Gitlab server's information.
  \item Search the database content.
\end{itemize}

The information stored by the application should be erasable by the owner of the Gitlab server for legal reason, as they are their property.
The easiest way to do that is to create a user account while someone want to add a server, this account will have the rights to edit the API's key and to remove the server from the database.
During the creation of this kind of account the user needs to give the permission to store the information of the Gitlab server.

Finally for ethical reason the application should several seconds between two query on the same server and don't refresh the information more than once a day to avoid any overload.

As this software is a web application the main limitation of it will be the server's capacity to handle a high traffic, moreover as the application will only be able to request the registered servers the usefulness of it depend mostly on the numbers of server in the database.

\newpage
\section{Literature review}
\subsection{Introduction}
Besides the creation of the search engine this project will also contain a study on the implementation of the trie data structure in Haskell and which kind of database is the more suitable for this structure.
The databases will be MariaDB, a relational database, Redis, a key-value database and acid-state a NoSQL database written in haskell.

The difference between those three database architectures can affect the speed needed to retrieve a specific string in a database containing a huge amount of information.
In our case the application needs to retrieve information about several projects or users stored in multiple Gitlab servers.
Depending on the number of stored projects and users the search engine which will use a trie for searching among the data needs to be the quicker possible to improve the user experience of the application.

This literature review is going to explain in a first phase the interest of using a trie as data structure, it's functioning and it's weakness.
Then we will see how this data structure is working on several databases architectures.

 \subsection{The trie data structure}
 A trie is a tree data structure used originally for working with strings indexed by keys but has later been widespread to any king of term \cite{ConnellyRichardH1995Agot}.

 At first created for low-speed memory medium \cite{DeLaBriandais:1959:FSU:1457838.1457895} the idea was to overcome the device's limitations by storing a keyword and the location of the corresponding records, thereby reducing the amount of processed data therefore decreasing the number of interactions with the memory. Although this limitation rarely apply nowadays this data structure is still efficient and can give fast results for a dictionary search.

 For example given the following set of strings stored in a database : abb, abc, abcb, bc, bbc, bba. The corresponding tree will be the following :

 \begin{center}
   \includegraphics[scale=0.4]{trie}\\
   \label{trie for the given case}
 \end{center}
By using this data structure we can check quickly if a given string is present in our set and which child exist. If a user input the string ac the trie can stop the search as it does not contain a string starting by ac.

Several improvement of the trie has been created, for example the use of a double-array has been used to speed up the looking process.
Following this implementation a double-array structure with label array has proven to be a good solution to improve speed and memory use \cite{KandaShunsuke2015Tcru}.

Another good way to improve the performance of this data structure is to use concurrency with snapshots. The snapshots are a good way to implement operations that use the entire information of the data structure as they represent the state of this data structure at a specific point.
This solution can be optimal for functional programming languages as they provide an efficient way to create the snapshots \cite{Prokopec:2012:CTE:2370036.2145836}.

Two kinds of database are going to be used in this project, relational database of NoSQL one.
 A relational database is a database which the design is based on the relational model of data, widely used in the web industry it's one of the most common database structure in software development.
 Although rarely used for this kind of application a relational database can be the structure of the database for a search engine \cite{PapadakosPanagiotis2009ODRf}.

On the other hand NoSQL databases has been widely used for handling large dictionary but still lake of some features that need to be implemented to create the perfect solution \cite{KimChulyun2015Ssji}.

 \subsection{Conclusion}
 The trie data structure fit the needs of this search engine as it has proven to be efficient while working on a set of strings shaping a fixed dictionary. The database of this project while therefore implement it by storing firstly a project or user and the information gathered on it. Then each string that can be searched will be stored with the location of the previously stored element.
 The details of implementations will vary from one database to another.

 The NoSQL structure seems at first more logical than of SQL one to implement this solution but the study will determine if it's the case. Moreover an hybrid solution can also be considered.

Finally a good way to improve the speed of the search engine will be to use multi-threading which can be done using a specific framework \cite{Qin:2013:TSS:2457317.2457389}.
\newpage

\section{Progress and achievement}
The application described in this document has already started to be created, however unlike the project plan included in the dissertation proposal I came to the conclusion to quickly create the backbone of the application rather than wait for each script to be written.
This choice has been reinforced by the feedbacks on the proposal as the timeline division, which consisted of creating each script entirely then finally creating the interface, has been considered too straightforward.

The current project contain a minimal version of the three scripts described before in this document and a small web interface done with Scotty.
With the local Gitlab server I have been able to ascertain which available token was the best for this case which seems to be the personal access one. Furthermore the query has been able to reach the server using either HTTP or HTTPS protocols with the help of the HTTP-Streams module.

In order to simplify the modules installation process Cabal, the Haskell's package manager has been used for each dependency.

This change has been made during the development as the new solution make it easier to test each script therefore to work on them.

The current application is able to work with each databases : MariaDB, Reddis and acid-state. The installation of each database has been done on my local computer.
Again Cabal has been used for acid-state, MariaDB has been installed using the Linux version of the software while Docker has been used to install Redis as it turn out to be easier to set up.

The milestones of the project plan are now the following :
\begin{itemize}
  \item Creation of a simple yet functional version of the search engine.
  \item Running the databases tests.
  \item Implementing the solution chosen with the help of the study.
  \item Improve the security and speed of the search engine.
\end{itemize}
\newpage
\bibliographystyle{IEEEtran}
\bibliography{Interim}
\end{document}
