\documentclass{scrartcl}
\usepackage{bold-extra}
\usepackage{graphicx}
\graphicspath{ {images/} }
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\chead{An Haskell search engine for open source projects}
\cfoot{\thepage}


\begin{document}
\title{An Haskell search engine for open source projects}
\subtitle{Oxford Brookes University - Supervisor: Dr. Ian Bayley}
\date{2017}
\author{Girardo Florian}
\maketitle


\newpage
\tableofcontents
\newpage
\section{Introduction}
With the rising popularity of the web interface for git repository Github and Bitbucket open source projects can now easily share their source code and manage the community involvement. However, one criticism often made by the open source community is that the sources of Github itself are closed.
As an alternative several open source git web servers have been created recently, the most known and used is probably Gitlab. Although lots of project switch to Gitlab and some website offer free access to a Gitlab server those solutions lack of the visibility that Github can provide as each server can only list the projects that it host.
This project aim to increase this visibility by creating a search engine able to index the content of many Gitlab servers.

This engine will be written using the Haskell language.
As we want a software that can responds quickly to the queries asked by an user this project is going to use the concurrency provided by the language.
A comparison between several database types will also be conducted with this objective in mind.
Finally this search engine is going to need an interface for the user to interact with. This interface is going to be done with a web module for Haskell, therefore the application will need to run on a server.

The activities of the project can be split in two steps, firstly it should be able to grab the information from a Gitlab server and store it in the database.
Secondly the software should process a query entered by the user and retrieve from the database the information that needs to be sorted then printed to the user.
Optionally, it could be interesting to query Github to create a single research page.

The deliverable will consist in the search engine able to carry out the features detailed previously and a document explaining the choices made during the development phase but also a study of advantages and drawback of different database types for this case.

\section{Choice of the subject}
An interest in functional programming language seems to rise recently in the professional world and company like Facebook started to use this paradigm for project like Haxl \cite{haxl}.
Furthermore some new languages like Scala \cite{scala} that combine several paradigm including the functional one strengthen the idea that learning a functional language can only be beneficial.
The module P00400 (Paradigms of programming) was using the Haskell programming language to introduce the functional concept which leads me to use the same language for the main project of the module.
This project helped me to understand the basics syntax and concept of the language nonetheless it also gave me the impression that Haskell is a powerful language while used in depth.

As the project for the paradigms of programming was a video game using a library for a graphical interface I chose this time to create a software using a web library to create on online interface in order to learn different facet of Haskell, moreover as I'm thinking of working in the web area after my studies this dissertation is a good opportunity to add one more arrow in the quiver.

\section{Objectives}
As said before the main objective of this project is to create a search engine to increase the visibility of projects hosted on several Gitlab servers.
This will be done by gathering information on those projects thanks to the Gitlab's API. As we want from the engine to be quick a lot of the research side will be done to achieve this goal.
During this dissertation a study of several databases will be done, the main criterion is going to be the speed but the ease of use will also be discussed, this will allow us to ascertain the most suitable database for a search engine in Haskell.
The databases that will be tested are a SQL database and two NoSQL databases. MySQL will be the SQL one, while for the two other we are going to use Redis which is a well known NoSQL database written in C but also a smaller one written in Haskell named acid-states \cite{acid}.

The software will be composed by several parts. The first one will be the skeleton that will be done using a web framework. This framework will allow us to route the other part of the software to specific URL.
From this interface the user should be able to request the indexing of the content of a Gitlab server simply by giving its root address, this will call the indexing script.
The script is going to call the API on the server in order to gather the needed information on the projects ans users then store them in the database. This script is going to use concurrency during the gathering stage (if the server can manage it) and during the storing one.
Still from the web interface the user should be able to ask for a specific project or user.
Ideally the search engine should be able to search the user query on Github and print the result alongside the indexed content.

Once this project fully completed it could be useful for some companies to find an open-source software that fit their needs, moreover the logic behind can be reuse to create a search engine for any kind of information.

\section{Literature review}
Writing a search engine is not something uncommon as they represent a way to filter information and are a common tool for our daily use of the web.
A search engine with the same goal as the one depicted in this document does not seems to exist however the essential part of this project is the use of functional programming language to create this kind of software.
Some module for Haskell are already available to create search engines \cite{holumbus} but can only use a SQL database, as a major part of this dissertation rest upon the test of several types of database this option is not suitable.

As no other search engine is available for this language the best option is to create a custom one, in order to do that I'm going to use a book explaining several approach on how to create a search engine \cite{searchEngine}.
This book cover the entire process of creating a search engine from the data to the user interface which fit perfectly my needs for this project.
As well as this book I'm going to use an article about depth-first search algorithm in Haskell \cite{depth} for the purpose of testing several search algorithms.

\section{Method and resources}
As it's my main operating system the development will be done on a Linux computer, on which Haskell run without any trouble. On top of the Haskell programming language I'm going to use Cabal \cite{cabal}, a package manager that will help to install any dependencies in a simple manner.
A local Gitlab server is going to be used, in order to make the process easy, this installation will be done via Docker as an official Docker image is available on the Gitlab website.

The web interface will be created using a web framework for Haskell still undefined for now, the choice will be done during the development stage as a web framework can be more suitable for a specific database than an other one.
To improve the management of the source code a git repository will help me to do the versioning.

As the we want a robust software I'm going to use a unitary testing framework. I have already worked several time with Ruby and the Rspec test framework this is the main reason that helps me choose the Hspec test framework for Haskell inspired by Rspec \cite{hspec}.

For the development side this project will be split in four scripts all linked by the web user interface :
\begin{enumerate}
  \item API requesting script : It will be run every time a user add a new server url and should be able to check if the provided url is a Gitlab server to grab the public information of the server. This script should be able to run automatically to update the information.
  \item Database storing script : Get the data from the previous script, sort and store them in the database.
  \item Database retrieving script : Whenever a user input a query this script is going to resolve it and grab the right information from the database.
  \item Information ordering script : Get the date from the previous script, sort them depending on the will of the user.
\end{enumerate}

As said before the tested databases are going to be MySQl, Redis and Acid-static.The tests are going to follow this scenario for each database :
\begin{itemize}
  \item Get the content of a small Gitlab server and store it in the database (1 - 4 projects/users).
  \item Get the content of a medium Gitlab server and store it in the database (10 - 20 projects/users).
  \item Get the content of a large Gitlab server and store it in the database (100 - 150 projects/users).
  \item Retrieve the information for a non specific query, small amount of criteria provided in order to get a big amount of results.
  \item Retrieve the information for a very specific query, large amount of criteria provided in order to get a very small amount of results.
\end{itemize}

Each task will be timed and run several time to create two chart, the first one being the average of storing speed while the second one will be the average for retrieving speed.
\section{Project plan}
This project last four months, so the Gantt chart of the whole project is here split every month to improve the readability.
The tasks are :
\begin{itemize}
  \item Gitlab instalation : setting up the docker container.
  \item API requesting script : writing the script able to request information from the container.
  \item MySQL database installation : setting up the MySQL database.
  \item Redis database installation : setting up the Redis database.
  \item Acid-state database installation : setting up the Acid-state database.
  \item Database storing script : writing a script that can interact with each database by storing data.
  \item Database retrieving script : writing a script able to interact with each database by retrieving data.
  \item Ordering information script : writing a script able to order data from the database based on the will of the user.
  \item Web interface : writing a web interface on which the user can interact and run all previously written scripts.
  \item Software finished (Milestone) : at this step the software should be working.
  \item Optimisations : writing some minor changes on the software for either improve the speed or the ease of use.
  \item Database study : doing all the tests for each database.
  \item Proposal : writing this document.
  \item Interim report : writing the interim report.
  \item Presentation support : creating a support for the presentation.
  \item Presentation (Milestone).
  \item Short paper : writing the short paper.
  \item Final report : writing the final report.
  \item Report revision : writing some minor changes on the final report.
\end{itemize}

Each development tasks contain also the time kept for the writing of the tests.

\subsection{Gantt chart}
\begin{center}
  \includegraphics[scale=0.36]{Gantt1}\\
  \label{Gantt chart june}
  \includegraphics[scale=0.36]{Gantt2}\\
  \label{Gantt chart july}
  \includegraphics[scale=0.37]{Gantt3}\\
  \label{Gantt chart august}
  \includegraphics[scale=0.37]{Gantt4}\\
  \label{Gantt chart september}
\end{center}
\subsection{Pert chart}
\begin{center}
  \includegraphics[scale=0.33]{pert}\\
  \label{Pert chart}
\end{center}
\bibliographystyle{IEEEtran}
\bibliography{DissertationProposal}
\end{document}
