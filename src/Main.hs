{-# LANGUAGE OverloadedStrings #-}
import qualified Control.Monad as C
import Control.Monad.IO.Class
import Crypto.PasswordStore
import qualified Data.ByteString.Char8 as C
import Data.Maybe
import qualified Data.Text.Lazy as LZ
import qualified Data.VCache.Trie as T
import Data.Word
import Database.VCache (VCache)
-- For static directory
import Network.Wai.Middleware.Static
import Text.Blaze.Html.Renderer.Text
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import Web.Cookie
import Web.Scotty
import Web.Scotty.Cookie
import qualified Web.Scotty as S

-- Local  modules
import CustomTypes
import DbSql
import qualified Query as Q
import Research
import Session
import Trie

main = do
  subDir <- vCacheInit
  tries <- createTries subDir
  scotty 3000 $ do
    -- Setting the "static" directory as the root
    S.middleware $ staticPolicy (noDots >-> addBase "../static")
    -- Generate key for session storing
    get "/" $ do
      pageIndex
    get "/addServer" $ do
      pageAddServer
    get "/crawlServers" $ do
      pageCrawlServer
    get "/install" $ do
      state <- liftIO $ initDatabase
      case state of
        True -> pageInstall "Done"
        False -> pageInstall "An error occured"
    get "/login" $ do
      pageLogin
    get "/register" $ do
      pageRegister
    get "/servers" $ do
      servers <- liftIO $ getAllServers
      pageServers servers
    post "/checkUser" $ do
      pageCheckUser
    post "/createServer" $ do
      pageCreateServer
    post "/createUser" $ do
      pageCreateUser
    post "/extendedRequestResult" $ do
      pageExtendedRequestResult
    post "/requestResult" $ do
      pageRequestResult tries
    notFound $ do
      pageNotFound

-- checkCookie :: Bool
-- checkCookie = do
--   cookie <- getCookie "Session"
--   if (isJust cookie)
--     then do
--       valueList <- splitCookieValue (LZ.unpack cookie)
--       userId <- head valueList
--       token <- concat (tail valueList)
--       return $ checkForSession userId token
--     else return False


-- PAGES
-- The pages functions are the one doing the interactions with the database, the views rendering and the redirections
-- Each page is called by the main function whenever a user visits a specific url

-- Render the view for the "/addServer" url
pageAddServer :: ActionM ()
pageAddServer = do
  S.html . renderHtml $ do
    viewAddServer

-- Check if the information provided by a user on the connection form is correct, connect the user if it is otherwise the user user redirected
pageCheckUser :: ActionM ()
pageCheckUser = do
  email <- (param "email") `rescue` (\msg -> return msg)
  password <- (param "password") `rescue` (\msg -> return msg)
  userPassword <- liftIO $ getUserPasswordByEmail (LZ.unpack email)
  if (verifyPassword (C.pack (LZ.unpack password)) (C.pack userPassword))
    -- The user can be connected
    then do
      userId <- liftIO $ getUserIdByEmail (LZ.unpack email)
      token <- liftIO $ createToken
      session <- liftIO $ insertSession (userId, token)
      let value = (show userId) ++ " " ++ token
      setSimpleCookie (LZ.toStrict (LZ.pack "Session")) (LZ.toStrict (LZ.pack value))
      -- setCookie $ createCookie (C.pack (show userId)) (C.pack token)
      redirect "/"
    else redirect "/login"

-- Send a request to crawl the servers stored in the database
pageCrawlServer :: ActionM ()
pageCrawlServer = do
  query <- liftIO $ Q.queryAllServers
  S.html . renderHtml $ do
    viewCrawlServer

-- Check the content of a new server form and create it if possible
pageCreateServer :: ActionM ()
pageCreateServer = do
  domain <- (param "domain") `rescue` (\msg -> return msg)
  location <- (param "location") `rescue` (\msg -> return msg)
  port <- (param "port") `rescue` (\msg -> return msg)
  token <- (param "token") `rescue` (\msg -> return msg)
  server <- liftIO $ checkForAddingServer (Just (gitlabServerFromTexts domain location port token))
  redirect "/servers"

-- Check the information provided by a user on the register form is correct, register the user if it is otherwise the user user redirected
pageCreateUser :: ActionM ()
pageCreateUser = do
  email <- (param "email") `rescue` (\msg -> return msg)
  password <- (param "password") `rescue` (\msg -> return msg)
  password2 <- (param "password2") `rescue` (\msg -> return msg)
  if (password == password2)
    then do
      safePassword <- liftIO $ makePassword (C.pack (LZ.unpack password)) 17
      user <- liftIO $ checkForAddingUser (Just (User (LZ.unpack email) (C.unpack safePassword) ))
      redirect "/"
    else redirect "/register"

-- Render the index view
pageIndex :: ActionM ()
pageIndex = do
  S.html . renderHtml $ do
    viewIndex

-- Render the install view
pageInstall :: String -> ActionM ()
pageInstall state = do
  S.html . renderHtml $ do
    viewInstall state

-- Render the login view
pageLogin :: ActionM ()
pageLogin = do
  S.html . renderHtml $ do
    viewLogin

-- Render the not found view
pageNotFound :: ActionM ()
pageNotFound = do
  S.html . renderHtml $ do
    viewNotFound

-- Render the register view
pageRegister :: ActionM ()
pageRegister = do
  S.html . renderHtml $ do
    viewRegister

-- Show the results of a research done by a user using the SQL search
pageExtendedRequestResult :: ActionM ()
pageExtendedRequestResult = do
  research <- (param "research") `rescue` (\msg -> return msg)
  matchingProjects <- liftIO $ getResearchResultExtended (LZ.unpack research)
  S.html . renderHtml $ do
    viewRequestResult (matchingProjects, (LZ.unpack research))

-- Show the results of a research done by a user using the B-tree search
pageRequestResult :: (T.Trie Int, T.Trie Int) -> ActionM ()
pageRequestResult tries = do
  research <- (param "research") `rescue` (\msg -> return msg)
  matchingProjects <- liftIO $ getResearchResultSimple (LZ.unpack research) tries
  S.html . renderHtml $ do
    viewRequestResult (matchingProjects, (LZ.unpack research))

-- Show all servers stored in the database
pageServers :: [GitlabServer] -> ActionM ()
pageServers servers = do
  S.html . renderHtml $ do
    viewServers $ map (\x -> gitlabServerPublicInfoAsString x) servers

-- TEMPLATES
-- The templates are bits of code that are often needed in several views,

-- Print the top menu template, the website's name and call the templateTopMenu
templateBodyHeader :: String -> H.Html
templateBodyHeader pageTitle = H.docTypeHtml $ do
  H.div H.! A.class_ "small-12 columns" $ do
    templateTopMenu
    H.div H.! A.class_ "text-center" $ do
      H.h1 "Gitlab search engine"
      H.h2 $ H.toHtml pageTitle
    templateJsFiles

-- Call the css files of the website
templateCssFiles :: H.Html
templateCssFiles = H.docTypeHtml $ do
  H.link H.! A.href "/css/app.css" H.! A.rel "stylesheet" H.! A.type_ "text/css"
  H.link H.! A.href "/css/foundation.css" H.! A.rel "stylesheet" H.! A.type_ "text/css"
  H.link H.! A.href "/css/font-awesome.min.css" H.! A.rel "stylesheet" H.! A.type_ "text/css"

-- Print the info of a given GitlabProject
templateGitlabProject :: GitlabProject -> H.Html
templateGitlabProject project = do
  H.div H.! A.class_ "small-11 columns float-center" $ do
    H.div H.! A.class_ "card" $ do
      H.div H.! A.class_ "card-divider" $ do
        H.h4 $ H.toHtml $ gitlabProjectName project
      H.div H.! A.class_ "card-section" $ do
        H.p $ H.toHtml $ gitlabProjectDescription project
        H.div H.! A.class_ "row" $ do
          H.section $ do
            H.i H.! A.class_ "fa fa-tags" $ "  "
            mapM_ (\x -> templateGitlabProjectTag x) (gitlabProjectTags project)
          H.section $ do
            H.i H.! A.class_ "fa fa-star" $ do
              " "
              H.toHtml $ gitlabProjectStars project
          H.section $ do
            H.i H.! A.class_ "fa fa-link" $ " "
            H.a H.! A.href (H.stringValue (gitlabProjectSshUrl project)) $ " SSH"
            H.a H.! A.href (H.stringValue (gitlabProjectHttpUrl project)) $ " HTTP"

-- -- Print a tag for a GitlabProject
templateGitlabProjectTag :: ProjectTag -> H.Html
templateGitlabProjectTag tag = H.docTypeHtml $ do
  H.span H.! A.class_ "label" $ H.toHtml $ projectTagName tag

-- Call the js files of the website
templateJsFiles :: H.Html
templateJsFiles = H.docTypeHtml $ do
  H.script H.! A.src "/js/vendor/jquery.js" $ mempty
  H.script H.! A.src "/js/vendor/what-input.js" $ mempty
  H.script H.! A.src "/js/vendor/foundation.min.js" $ mempty
  H.script H.! A.src "/js/app.js" $ mempty

-- Print the head of each page of the website, it contain the needed meta tags
templatePageHeader :: H.Html
templatePageHeader = H.docTypeHtml $ do
  H.head $ do
    templateCssFiles
    H.title "Gitlab search engine"
    H.meta H.! A.charset "utf-8"
    H.meta H.! A.httpEquiv "x-ua-compatible" H.! A.content "ie=edge"
    H.meta H.! A.name "viewport" H.! A.content "width=device-width, initial-scale=1.0"
    H.link H.! A.href "/images/favicon.png" H.! A.rel "shortcut icon" H.! A.type_ "image/png"

-- Print the search form
templateResearch :: H.Html
templateResearch = H.docTypeHtml $ do
  H.form H.! A.action "/requestResult" H.! A.method "post" $ do
    H.ul H.! A.class_ "menu align-center" $ do
      H.li $ H.input H.! A.type_ "search" H.! A.name "research" H.! A.placeholder "Your research"
      H.li $ H.button H.! A.type_ "submit" H.! A.class_ "button" $ "Search"

-- Format the given list of string for creating a line of table
templateTableRow :: [String] -> H.Html
templateTableRow content = H.docTypeHtml $ do
  H.div H.! A.class_ "large-8 small-11 columns float-center" $ do
    H.tr $ C.forM_ content (H.td . H.toHtml)

-- Print the top menu with the links to the main url
templateTopMenu :: H.Html
templateTopMenu = H.docTypeHtml $ do
  H.div H.! A.class_ "top-bar" $ do
    H.div H.! A.class_ "top-bar-left" $
      H.ul H.! A.class_ "menu" $ do
        H.li H.! A.class_ "menu-text" $ H.a H.! A.href "/" H.! A.class_ "nopadding" $ "Gitlab Search"
        H.li $ H.a H.! A.href "/servers" $ "Servers"
        H.li $ H.a H.! A.href "/addServer" $ "Add server"
        H.li $ H.a H.! A.href "/register" $ "Register"
        H.li $ H.a H.! A.href "/login" $ "Login"
    H.div H.! A.class_ "top-bar-left" $
      templateResearch

-- VIEWS
-- The views are functions that print the information in a readable way, using Foundation they also provide a good experience for any screen' size

-- Print a form for adding a server to the database
viewAddServer :: H.Html
viewAddServer = H.docTypeHtml $ do
  templatePageHeader
  H.body $ do
    H.div H.! A.class_ "grid-container" $ do
      H.div H.! A.class_ "grid-x grid-padding-x" $ do
        templateBodyHeader "Add server"
        H.div H.! A.class_ "large-8 small-11 columns float-center" $ do
          H.form H.! A.action "/createServer" H.! A.method "post" $ do
              H.label $ do
                "Domain"
                H.input H.! A.type_ "text" H.! A.name "domain" H.! A.placeholder "Server's domain"
              H.label $ do
                "Location"
                H.input H.! A.type_ "text" H.! A.name "location" H.! A.placeholder "Server's location"
              H.label $ do
                "Port"
                H.input H.! A.type_ "text" H.! A.name "port" H.! A.placeholder "Server's port"
              H.p H.! A.class_ "help-text" H.! A.id "portHelpText" $ "Default : 80"
              H.label $ do
                "Token"
                H.input H.! A.type_ "text" H.! A.name "token" H.! A.placeholder "Server's token"
              H.input H.! A.type_ "submit" H.! A.class_ "button"

--
viewCrawlServer :: H.Html
viewCrawlServer = H.docTypeHtml $ do
  templatePageHeader
  H.body $ do
    H.div H.! A.class_ "grid-container" $ do
      H.div H.! A.class_ "grid-x grid-padding-x" $ do
        templateBodyHeader "Manual servers crawling"

-- Print the index page with the search form
viewIndex :: H.Html
viewIndex = H.docTypeHtml $ do
  templatePageHeader
  H.body $ do
    H.div H.! A.class_ "grid-container" $ do
      H.div H.! A.class_ "grid-x grid-padding-x" $ do
        templateBodyHeader "Search"
        H.div H.! A.class_ "large-8 small-11 columns float-center" $ do
          templateResearch

-- Print the status given by the database after creating the tables
viewInstall :: String -> H.Html
viewInstall state = H.docTypeHtml $ do
  templatePageHeader
  H.body $ do
    H.div H.! A.class_ "grid-container" $ do
      H.div H.! A.class_ "grid-x grid-padding-x" $ do
        templateBodyHeader "Installation"
        H.p $ do
          "State : "
          H.toHtml state

-- Print a form for loging a user
viewLogin :: H.Html
viewLogin = H.docTypeHtml $ do
  templatePageHeader
  H.body $ do
    H.div H.! A.class_ "grid-container" $ do
      H.div H.! A.class_ "grid-x grid-padding-x" $ do
        templateBodyHeader "Login"
        H.div H.! A.class_ "large-8 small-11 columns float-center" $ do
          H.form H.! A.action "/checkUser" H.! A.method "post" $ do
              H.label $ do
                "Email"
                H.input H.! A.type_ "text" H.! A.name "email" H.! A.placeholder "Enter your e-mail"
              H.label $ do
                "Password"
                H.input H.! A.type_ "password" H.! A.name "password" H.! A.placeholder "Enter your password"
              H.input H.! A.type_ "submit" H.! A.class_ "button"

-- Print the 404 page
viewNotFound :: H.Html
viewNotFound = H.docTypeHtml $ do
  templatePageHeader
  H.body $ do
    H.div H.! A.class_ "grid-container" $ do
      H.div H.! A.class_ "grid-x grid-padding-x" $ do
        templateBodyHeader "404 - Page not found"

-- Print a form for registering a user
viewRegister :: H.Html
viewRegister = H.docTypeHtml $ do
  templatePageHeader
  H.body $ do
    H.div H.! A.class_ "grid-container" $ do
      H.div H.! A.class_ "grid-x grid-padding-x" $ do
        templateBodyHeader "Register"
        H.div H.! A.class_ "large-8 small-11 columns float-center" $ do
          H.form H.! A.action "/createUser" H.! A.method "post" $ do
              H.label $ do
                "Email"
                H.input H.! A.type_ "text" H.! A.name "email" H.! A.placeholder "Enter your e-mail"
              H.label $ do
                "Password"
                H.input H.! A.type_ "password" H.! A.name "password" H.! A.placeholder "Enter your password"
              H.label $ do
                "Repeat password"
                H.input H.! A.type_ "password" H.! A.name "password2" H.! A.placeholder "Re-enter your password"
              H.input H.! A.type_ "submit" H.! A.class_ "button"

-- Print the result of a user's research
viewRequestResult :: ([GitlabProject], String) -> H.Html
viewRequestResult (projects, research) = H.docTypeHtml $ do
  templatePageHeader
  H.body $ do
    H.div H.! A.class_ "grid-container" $ do
      H.div H.! A.class_ "grid-x grid-padding-x" $ do
        templateBodyHeader "Search result"
        mapM_ (\x -> templateGitlabProject x) projects
        H.div H.! A.class_ "large-8 small-11 columns float-center" $ do
          H.form H.! A.action "/extendedRequestResult" H.! A.method "post" $ do
            H.ul H.! A.class_ "menu align-center" $ do
              H.li $ H.input H.! A.type_ "hidden" H.! A.name "research" H.! A.value (H.stringValue research)
              H.li $ H.button H.! A.type_ "submit" H.! A.class_ "button" $ "Extended research"

-- Print all servers from the database
viewServers :: [[String]] -> H.Html
viewServers servers = H.docTypeHtml $ do
  templatePageHeader
  H.body $ do
    H.div H.! A.class_ "grid-container" $ do
      H.div H.! A.class_ "grid-x grid-padding-x" $ do
        templateBodyHeader "Registered servers"
        H.div H.! A.class_ "large-8 small-11 columns float-center" $ do
          H.table $ do
            H.thead $ do
              H.tr $ do
                H.th $ "domain"
                H.th $ "location"
                H.th $ "port"
            H.tbody $ mapM_ (\x -> templateTableRow x) servers
