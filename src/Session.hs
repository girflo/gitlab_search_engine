module Session where
  import qualified Data.ByteString as B
  import qualified Data.ByteString.Char8 as C
  import Data.List.Split
  import Data.Time.Clock (secondsToDiffTime)
  import Test.RandomStrings
  import Web.ClientSession
  import Web.Cookie

  -- Create a sessionId
  createToken :: IO String
  createToken = randomString randomChar 32

  -- createCookie :: B.ByteString -> B.ByteString -> SetCookie
  -- createCookie userId token = def {setCookieName = userId, setCookieValue = token, setCookieMaxAge = (Just (secondsToDiffTime 10800)), setCookieHttpOnly = False, setCookieSecure = False}

  -- Create a secure cookie
  createCookie :: B.ByteString -> B.ByteString -> SetCookie
  createCookie userId token = def {setCookieName = userId, setCookieValue = token}

  -- Split the userId and the sessionId from the cookie value
  splitCookieValue :: String -> [String]
  splitCookieValue value = splitOn " " value
