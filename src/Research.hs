module Research where
  import qualified Data.VCache.Trie as T
  import Database.VCache
  import Data.ByteString.Char8 (pack)

  -- Local module
  import CustomTypes
  import DbSql
  import Trie

  -- Create two tries, the first one containing the Names of projects in db the second one containing the descriptions
  createTries :: VCache -> IO (T.Trie Int, T.Trie Int)
  createTries subDir = do
    trieWithNames <- populateTrieWithProjectsNames $ createEmptyTrie subDir
    trieWithDescriptions <- populateTrieWithProjectsDescriptions $ createEmptyTrie subDir
    return (trieWithNames, trieWithDescriptions)

  -- Research the given string for all projects' descriptions and name
  getResearchResultSimple :: String -> (T.Trie Int, T.Trie Int) -> IO ([GitlabProject])
  getResearchResultSimple research tries = do
    idMatchingProjects <- searchTries research tries
    matchingProjects <- getGitlabProjectByIds idMatchingProjects
    return matchingProjects

  -- Insert all projects names in db inside the given trie
  populateTrieWithProjectsNames :: T.Trie Int -> IO (T.Trie Int)
  populateTrieWithProjectsNames trie = do
    projectsNames <- getAllProjectsNames
    let finalProjectsNamesList = map (\ x -> ((pack (snd x), fst x))) projectsNames
    return $ insertListInTrie finalProjectsNamesList trie

  -- Insert all projects descriptions in db inside the given trie
  populateTrieWithProjectsDescriptions :: T.Trie Int -> IO (T.Trie Int)
  populateTrieWithProjectsDescriptions trie = do
    projectsDescriptions <- getAllProjectsDescriptions
    let finalProjectsDescriptionList = map (\ x -> ((pack (snd x), fst x))) projectsDescriptions
    return $ insertListInTrie finalProjectsDescriptionList trie

  -- Search function using the haskell way
  searchHaskell :: String -> IO ([Int],[Int])
  searchHaskell research = do
    researchHaskellResultDescription <- searchHaskellDescription research
    researchHaskellResultName <- searchHaskellName research
    return (researchHaskellResultDescription, researchHaskellResultName)

  -- Search function using the SQL way
  searchSql :: String -> IO ([Int],[Int])
  searchSql research = do
    researchSqlResultDescription <- searchSqlDescription research
    researchSqlResultName <- searchSqlName research
    return (researchSqlResultDescription, researchSqlResultName)

  -- Search function using the B-tree way
  searchBTree :: String -> IO ([Int],[Int])
  searchBTree research = do
    researchBTreeResultDescription <- searchBTreeDescription research
    researchBTreeResultName <- searchBTreeName research
    return (researchBTreeResultDescription, researchBTreeResultName)

  -- Search the given string within the two tries keys and return the matchings values
  searchTries :: String -> (T.Trie Int, T.Trie Int) -> IO ([Int])
  searchTries research tries = do
    trieNameResult <- searchTrie research (fst tries)
    trieDescriptionResult <- searchTrie research (snd tries)
    return (trieNameResult ++ trieDescriptionResult)

  -- Search the given string within the trie keys and return the matchings values
  searchTrie :: String -> T.Trie Int -> IO ([Int])
  searchTrie research trie = do
    let researchTrieResultName = T.lookupPrefix (pack research) trie
    let trieNameElems = T.elems researchTrieResultName
    return (trieNameElems)
