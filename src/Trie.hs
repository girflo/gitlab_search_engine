module Trie where
  import Data.ByteString.Char8
  import qualified Data.VCache.Trie as T
  import Database.VCache

  -- Construct an empty trie and return it
  createEmptyTrie :: VCache -> T.Trie a
  createEmptyTrie subDir = T.empty $ vcache_space subDir

  -- Insert the given list of tuple in the given trie
  insertListInTrie :: [(ByteString, Int)] -> T.Trie Int -> T.Trie Int
  insertListInTrie list trie = T.insertList list trie

  -- Transform a tuple holding one entry of the trie to a tuple that can be used by the matchingStringsIds function is the DbSql.hs file
  trieContentAsString :: (ByteString, Int) -> (Int, String)
  trieContentAsString trieContent = ((snd trieContent), (unpack(fst trieContent)))

  -- Return the entire content of a trie in a format that can be used by the matchingStringsIds function is the DbSql.hs file
  trieEntireContentAsString :: T.Trie Int -> [(Int, String)]
  trieEntireContentAsString trie = do
    let content = T.toList trie
    contentAsString <- Prelude.map (\x -> trieContentAsString x) content
    return contentAsString

  -- Open VCache with a maximum size in megabytes and file name
  vCacheInit :: IO VCache
  vCacheInit = do
    vCache <- openVCache 100 "db"
    return $ vCacheCreateSubdir vCacheSubdirName vCache

  -- Create a subdir in VCache with the given location
  vCacheCreateSubdir :: ByteString -> VCache -> VCache
  vCacheCreateSubdir location vcache = vcacheSubdir location vcache

  -- Return the name for the subdir of the application
  vCacheSubdirName :: ByteString
  vCacheSubdirName = pack "SearchEngine"
