{-# LANGUAGE OverloadedStrings #-}
module Query where
  import Data.Bool
  import qualified Data.ByteString as S
  import qualified Data.ByteString.Char8 as C
  import Data.Maybe
  import Network.Http.Client
  import System.IO.Streams (InputStream, OutputStream, stdout)
  import qualified System.IO.Streams as Streams

  -- Local modules
  import CustomTypes
  import DbSql
  import JSONParser

  -- Gather all servers' information and insert their projects
  queryAllServers :: IO ()
  queryAllServers = do
    servers <- getAllServers
    mapM_ serverRequest servers
    return ()

  -- Request a server's API and transform it into JsonProject
  requestToJSON :: S.ByteString -> S.ByteString -> S.ByteString -> S.ByteString -> IO  JsonProject
  requestToJSON domain port location token = do
    let url = requestUrl domain port location token
    v <- get url jsonHandler :: IO JsonProject
    return v

  -- Create an URL with the given server's information
  requestUrl :: S.ByteString -> S.ByteString -> S.ByteString -> S.ByteString -> URL
  requestUrl domain port location token = S.concat [domain, ":", port, "/api/v4/", location, "?private_token=", token]

  -- Get the JSON of the server's API and add the content to the database
  serverRequest :: GitlabServer -> IO ()
  serverRequest server = do
    jsonProjects <- requestToJSON (C.pack (gitlabServerDomain server)) (C.pack(gitlabServerPortAsString server)) (C.pack(gitlabServerLocation server)) (C.pack (gitlabServerToken server))
    serverId <- getServerIdByUrl (gitlabServerUrl server)
    insertProjectWithoutTag ((translateJsonProject jsonProjects), serverId)
    return ()
