module CustomTypes where
  import qualified Data.Text as T
  import qualified Data.Text.Lazy as LZ
  import Data.Word
  import JSONParser

  -- Parameters :  name description ssh_url http_url stars tags
  data GitlabProject = GitlabProject String String String String Int [ProjectTag] deriving (Show)

  -- Parameters :  name description ssh_url http_url stars
  data GitlabProjectWithoutTag = GitlabProjectWithoutTag String String String String Int deriving (Show)

  -- Parameters : id url token
  data GitlabServer = GitlabServer GitlabUrl String deriving (Show)

  -- Parameters : domain location port
  data GitlabUrl = GitlabUrl String String Word16 deriving (Show)

  -- Parameter :  name
  data ProjectTag = ProjectTag String deriving (Show)

  -- Parameters : email password
  data User = User String String deriving (Show)

  -- Return the description of a GitlabProject
  gitlabProjectDescription :: GitlabProject -> String
  gitlabProjectDescription (GitlabProject _ description _ _ _ _) = description

  -- Return the http_url of a GitlabProject
  gitlabProjectHttpUrl :: GitlabProject -> String
  gitlabProjectHttpUrl (GitlabProject _ _ _ http_url _ _) = http_url

  -- Return the name of a GitlabProject
  gitlabProjectName :: GitlabProject -> String
  gitlabProjectName (GitlabProject name _ _ _ _ _) = name

  -- Return the ssh_url of a GitlabProject
  gitlabProjectSshUrl :: GitlabProject -> String
  gitlabProjectSshUrl (GitlabProject _ _ ssh_url _ _ _) = ssh_url

  -- Return the stars number of a GitlabProject
  gitlabProjectStars :: GitlabProject -> Int
  gitlabProjectStars (GitlabProject _ _ _ _ stars _) = stars

  -- Return the tags of a GitlabProject
  gitlabProjectTags :: GitlabProject -> [ProjectTag]
  gitlabProjectTags (GitlabProject _ _ _ _ _ tags) = tags

  -- Return the description of a GitlabProjectWithoutTag
  gitlabProjectWithoutTagDescription :: GitlabProjectWithoutTag -> String
  gitlabProjectWithoutTagDescription (GitlabProjectWithoutTag _ description _ _ _) = description

  -- Return the http_url of a GitlabProjectWithoutTag
  gitlabProjectWithoutTagHttpUrl :: GitlabProjectWithoutTag -> String
  gitlabProjectWithoutTagHttpUrl (GitlabProjectWithoutTag _ _ _ http_url _) = http_url

  -- Return the name of a GitlabProjectWithoutTag
  gitlabProjectWithoutTagName :: GitlabProjectWithoutTag -> String
  gitlabProjectWithoutTagName (GitlabProjectWithoutTag name _ _ _ _) = name

  -- Return the ssh_url of a GitlabProjectWithoutTag
  gitlabProjectWithoutTagSshUrl :: GitlabProjectWithoutTag -> String
  gitlabProjectWithoutTagSshUrl (GitlabProjectWithoutTag _ _ ssh_url _ _) = ssh_url

  -- Return the stars number of a GitlabProjectWithoutTag
  gitlabProjectWithoutTagStars :: GitlabProjectWithoutTag -> Int
  gitlabProjectWithoutTagStars (GitlabProjectWithoutTag _ _ _ _ stars) = stars

  -- Return the domain of a GitlabServer
  gitlabServerDomain :: GitlabServer -> String
  gitlabServerDomain server = gitlabUrlDomain (gitlabServerUrl server)

  -- Create a GitlabServer from parameters in Text type
  gitlabServerFromTexts :: LZ.Text -> LZ.Text -> LZ.Text -> LZ.Text -> GitlabServer
  gitlabServerFromTexts domain location port token = GitlabServer (GitlabUrl (LZ.unpack domain) (LZ.unpack location) (fromInteger (read(LZ.unpack port)))) (LZ.unpack token)

  -- Return the location of a GitlabServer
  gitlabServerLocation :: GitlabServer -> String
  gitlabServerLocation server = gitlabUrlLocation (gitlabServerUrl server)

  -- Return the port of a GitlabServer as number
  gitlabServerPort :: GitlabServer -> Word16
  gitlabServerPort server = gitlabUrlPort (gitlabServerUrl server)

  -- Return the port of a GitlabServer as string
  gitlabServerPortAsString :: GitlabServer -> String
  gitlabServerPortAsString server = show (toInteger (gitlabUrlPort (gitlabServerUrl server)))

  -- Return the domain, location and port of a GitlabServer in a list
  gitlabServerPublicInfoAsString :: GitlabServer -> [String]
  gitlabServerPublicInfoAsString server = [(gitlabServerDomain server), (gitlabServerLocation server), (gitlabServerPortAsString server)]

  -- Return the token of a GitlabServer
  gitlabServerToken :: GitlabServer -> String
  gitlabServerToken (GitlabServer _ token) = token

  -- Return the url of a GitlabServer
  gitlabServerUrl :: GitlabServer -> GitlabUrl
  gitlabServerUrl (GitlabServer url _) = url

  -- Return the domain of a GitlabUrl
  gitlabUrlDomain :: GitlabUrl -> String
  gitlabUrlDomain (GitlabUrl domain _ _) = domain

  -- Return the location of a GitlabUrl
  gitlabUrlLocation :: GitlabUrl -> String
  gitlabUrlLocation (GitlabUrl _ location _) = location

  -- Return the port of a GitlabUrl
  gitlabUrlPort :: GitlabUrl -> Word16
  gitlabUrlPort (GitlabUrl _ _ port) = port

  -- Return the name of a ProjectTag
  projectTagName :: ProjectTag -> String
  projectTagName (ProjectTag name) = name

  -- Create a GitlabProjectWithoutTag with given name description ssh_url http_url stars; is used for translating values from the database
  translateGitlabProjectWithoutTagFromSql :: T.Text -> T.Text -> T.Text -> T.Text -> Int ->  GitlabProjectWithoutTag
  translateGitlabProjectWithoutTagFromSql name description ssh_url http_url stars = do
    GitlabProjectWithoutTag (T.unpack name) (T.unpack description) (T.unpack ssh_url) (T.unpack http_url) stars

  -- Create a GitlabProject with given GitlabProjectWithoutTag and ProjectTag list; is used for translating values from the database
  translateGitlabProjectAndTag :: GitlabProjectWithoutTag -> [ProjectTag] ->  GitlabProject
  translateGitlabProjectAndTag projectWithoutTag tags = do
    GitlabProject (gitlabProjectWithoutTagName projectWithoutTag) (gitlabProjectWithoutTagDescription projectWithoutTag) (gitlabProjectWithoutTagSshUrl projectWithoutTag) (gitlabProjectWithoutTagHttpUrl projectWithoutTag) (gitlabProjectWithoutTagStars projectWithoutTag) tags

  -- Create a GitlabServer (from the CustomTypes module) with given token, domain, location and port; is used for translating values from the database
  translateGitlabServerFromSql :: LZ.Text -> LZ.Text -> LZ.Text -> LZ.Text -> GitlabServer
  translateGitlabServerFromSql token domain location port =
    gitlabServerFromTexts domain location port token

  translateJsonProject :: JsonProject -> GitlabProjectWithoutTag
  translateJsonProject (JsonProject name description sshUrl httpUrl stars) =
    GitlabProjectWithoutTag (T.unpack name) (T.unpack description) (T.unpack sshUrl) (T.unpack httpUrl) stars

  -- Create a ProjectTag (from the CustomTypes module) with given name; is used for translating values from the database
  translateTagFromSql :: T.Text  -> ProjectTag
  translateTagFromSql name = do
    ProjectTag (T.unpack name)

  -- Create a User (from the CustomTypes module) with given email and password; is used for translating values from the database
  translateUserFromSql :: T.Text -> User
  translateUserFromSql email = do
    User (T.unpack email) ""
