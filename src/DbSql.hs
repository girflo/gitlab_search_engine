{-# LANGUAGE OverloadedStrings #-}
module DbSql where
  import Control.Monad
  import Data.List
  import Data.Word
  import qualified Data.Text as T
  import qualified Data.Text.Lazy as LZ
  import Database.MySQL.Simple
  import Database.MySQL.Simple.QueryResults
  import qualified Network.Http.Client as Net

  -- Local module
  import CustomTypes

  -- Check is the given Project's name and serverId is already stored in the database, if not calls the insertProject function
  checkForAddingProject :: (GitlabProject, Word64) -> IO ()
  checkForAddingProject ((GitlabProject name description ssh_url http_url stars tags), serverId) = do
    projectId <- getProjectIdByNameAndServerId (name, serverId)
    if (projectId == 0)
      then insertProject ((GitlabProject name description ssh_url http_url stars tags), serverId)
      else return ()

  -- Check is the given Server's url is already stored in the database, if not calls the insertServer function
  checkForAddingServer :: Maybe GitlabServer -> IO (Word64)
  checkForAddingServer Nothing = return (0)
  checkForAddingServer (Just (GitlabServer url token)) = do
    serverId <- getServerIdByUrl url
    if (serverId == 0)
      then insertServer (GitlabServer url token)
      else return serverId

  -- Check is the given session's id is already stored in the database, if not calls the insertSession function otherwhise return False
  -- checkForAddingSession :: (Word64, String) -> IO (Bool)
  -- checkForAddingSession (userId, sessionId) = do
  --   tagId <- getSessionById sessionId
  --   if (session == 0)
  --     then insertSession (userId, sessionId)
  --     else return False

  -- Check is the given tag's name is already stored in the database, if not calls the insertTag function otherwhise return the tag id
  checkForAddingTag :: ProjectTag -> IO (Word64)
  checkForAddingTag (ProjectTag name) = do
    tagId <- getTagIdByName name
    if (tagId == 0)
      then insertTag (ProjectTag name)
      else return tagId

  -- Check is the given user's email is already stored in the database, if not calls the insertUser function
  checkForAddingUser :: Maybe User -> IO ()
  checkForAddingUser Nothing = return ()
  checkForAddingUser (Just (User email password)) = do
    userNumber <- getUserByEmail email
    if ((length userNumber) == 0)
      then insertUser (User email password)
      else return ()

  checkForConnectingUser :: Maybe User -> IO (Int)
  checkForConnectingUser Nothing = return (0::Int)
  checkForConnectingUser (Just (User email password)) = do
    userExist <- getUserByEmailAndPassword email password
    return userExist

  checkForSession :: (String, String) -> IO Bool
  checkForSession (userId, sessionId) = do
    conn <- connection
    userSessions <- query conn "SELECT id from sessions where userId = ? and sessionId = ?"  (userId, sessionId)
    userSessionId <- forM userSessions $ \(Only id) -> return (id :: Word64)
    if ((head userSessionId) > 0)
      then return True
      else return False

  -- Return a function for connecting with the database
  connection :: IO Connection
  connection = connect
      defaultConnectInfo {connectUser = "haskell", connectPassword = "H-_8Mu=W7UNqzwxY{#", connectDatabase = "haskell"}

  -- Remove a session matchin the given id
  deleteSessionById :: Word64 -> IO()
  deleteSessionById id = do
    conn <- connection
    query <- execute conn "delete from sessions where id=?" (Only id)
    return ()

  -- Create all tables for the project, is performed when the user visits "/install"
  initDatabase :: IO Bool
  initDatabase = do
    let databasesCreated = False
    initTableServers
    initTableUsers
    initTableProjects
    initTableTags
    initTableProjectTags
    initTableSessions
    let databasesCreated = True
    return databasesCreated

  -- INIT FUNCTIONS
  -- Create the table for the projects from a gitlab server
  initTableProjects :: IO ()
  initTableProjects = do
    conn <- connection
    execute_ conn "CREATE TABLE IF NOT EXISTS projects (id int NOT NULL AUTO_INCREMENT, name varchar(255), description varchar(255), ssh_url varchar(255), http_url varchar(255), stars int, serverId int NOT NULL, PRIMARY KEY (id), FOREIGN KEY (serverId) REFERENCES servers(id));"
    return ()

  -- Create the table for project_tags storing the id of a project and a tag
  initTableProjectTags :: IO ()
  initTableProjectTags = do
    conn <- connection
    execute_ conn "CREATE TABLE IF NOT EXISTS project_tags (id int NOT NULL AUTO_INCREMENT, projectId int NOT NULL, tagId int NOT NULL, PRIMARY KEY (id), FOREIGN KEY (projectId) REFERENCES projects(id), FOREIGN KEY (tagId) REFERENCES tags(id));"
    return ()

  -- Create the table for the sessions
  initTableSessions :: IO ()
  initTableSessions = do
    conn <- connection
    execute_ conn "CREATE TABLE IF NOT EXISTS sessions (id int NOT NULL AUTO_INCREMENT, sessionId varchar(255) NOT NULL, userId int NOT NULL, PRIMARY KEY (id), FOREIGN KEY (userId) REFERENCES users(id));"
    return ()

  -- Create the table for the gitlab servers
  initTableServers :: IO ()
  initTableServers = do
    conn <- connection
    execute_ conn "CREATE TABLE IF NOT EXISTS servers (id int NOT NULL AUTO_INCREMENT, token varchar(255), domain varchar(255), port varchar(255), location varchar(255), PRIMARY KEY (id));"
    return ()

  -- Create the table for the tags of a project
  initTableTags :: IO ()
  initTableTags = do
    conn <- connection
    execute_ conn "CREATE TABLE IF NOT EXISTS tags (id int NOT NULL AUTO_INCREMENT, name varchar(255), PRIMARY KEY (id));"
    return ()

  -- Create the table for the user
  initTableUsers :: IO ()
  initTableUsers = do
    conn <- connection
    execute_ conn "CREATE TABLE IF NOT EXISTS users (id int NOT NULL AUTO_INCREMENT, email varchar(255), password varchar(255), PRIMARY KEY (id));"
    return ()

  -- INSERT FUNCTIONS
  -- Insert a given GitlabProject to the corresponding table
  insertProject :: (GitlabProject, Word64) -> IO ()
  insertProject ((GitlabProject name description ssh_url http_url stars tags), serverId) = do
    conn <- connection
    query <- execute conn "INSERT INTO projects (name, description, ssh_url, http_url, stars, serverId) VALUES (?, ?, ?, ?, ?, ?)" (name, description, ssh_url, http_url, stars, serverId)
    projectId <- insertID conn
    tagsId <- insertTagList tags
    forM tagsId $ \(tagId) -> insertProjectTag (projectId, tagId)
    return ()

  -- Insert a given GitlabProjectWithoutTag to the corresponding table
  insertProjectWithoutTag :: (GitlabProjectWithoutTag, Word64) -> IO ()
  insertProjectWithoutTag ((GitlabProjectWithoutTag name description ssh_url http_url stars), serverId) = do
    conn <- connection
    query <- execute conn "INSERT INTO projects (name, description, ssh_url, http_url, stars, serverId) VALUES (?, ?, ?, ?, ?, ?)" (name, description, ssh_url, http_url, stars, serverId)
    projectId <- insertID conn
    return ()

  -- Insert a project and tag id to the table project_tags
  insertProjectTag :: (Word64, Word64) -> IO ()
  insertProjectTag (projectId, tagId) = do
    conn <- connection
    query <- execute conn "INSERT INTO project_tags (projectId, tagId) VALUES (?, ?)" (projectId, tagId)
    return ()

  -- Insert a given userId and sessionId in the session table
  insertSession :: (Int, String) -> IO ()
  insertSession (userId, sessionId) = do
    conn <- connection
    userSessions <- query conn "SELECT id from sessions where userId = ?" (Only userId)
    forM userSessions $ \(Only id) -> deleteSessionById (id :: Word64)
    query <- execute conn "INSERT INTO sessions (userId, sessionId) VALUES (?, ?)" (userId, sessionId)
    return ()

  -- Insert a given ProjectTag to the corresponding table, return the id of the newly inserted tag
  insertTag :: ProjectTag -> IO (Word64)
  insertTag (ProjectTag name) = do
    conn <- connection
    -- We need to use a singleton list for substituting a single parametre with mysql_simple
    query <- execute conn "INSERT INTO tags (name) VALUES (?)" [name]
    tagId <- insertID conn
    return (tagId)

  -- Insert a list of tag and return a list of those tags' id
  insertTagList :: [ProjectTag] -> IO ([Word64])
  insertTagList tagList = do
    tagIdList <- forM tagList $ \(tag) -> checkForAddingTag tag
    return tagIdList

  -- Insert a given GitlabServer to the corresponding table
  insertServer :: GitlabServer -> IO (Word64)
  insertServer (GitlabServer url token) = do
    conn <- connection
    execute conn "INSERT INTO servers (token, domain, port, location) VALUES (?, ?, ?, ?)" (token, (gitlabUrlDomain url), (gitlabUrlPort url), (gitlabUrlLocation url))
    projectId <- insertID conn
    return (projectId)

  -- Insert a given User to the corresponding table
  insertUser :: User -> IO ()
  insertUser (User email password) = do
    conn <- connection
    query <- execute conn "INSERT INTO users (email, password) VALUES (?, ?)" (email, password)
    return ()

  -- Return all projects descriptions and ids stored in the database
  getAllProjectsDescriptions :: IO [(Int, String)]
  getAllProjectsDescriptions = do
    conn <- connection
    sqlGitlabProjects <- query_ conn ("SELECT id,description FROM projects")
    projectsIdsAndDescription <- forM sqlGitlabProjects $ \(id, name) -> return ((id :: Int), name)
    return projectsIdsAndDescription

  -- Return all projects names and ids stored in the database
  getAllProjectsNames :: IO [(Int, String)]
  getAllProjectsNames = do
    conn <- connection
    sqlGitlabProjects <- query_ conn ("SELECT id,name FROM projects")
    projectsIdsAndNames <- forM sqlGitlabProjects $ \(id, name) -> return ((id :: Int), name)
    return projectsIdsAndNames

  -- Return all servers stored in the database
  getAllServers :: IO [GitlabServer]
  getAllServers = do
    conn <- connection
    sqlGitlabServers <- query_ conn ("SELECT token,domain,location,port FROM servers")
    haskellGitlabServers <- forM sqlGitlabServers $ \(token,domain,location,port) -> return $ translateGitlabServerFromSql token domain location port
    return haskellGitlabServers

  -- Return all users stored in the database
  getAllUser :: IO [User]
  getAllUser = do
    conn <- connection
    sqlUsers <- query_ conn ("SELECT email FROM users")
    haskellUsers <- forM sqlUsers $ \(Only email) -> return $ translateUserFromSql email
    return haskellUsers

  -- Return the project matching the given id
  getProjectById :: Int -> IO (GitlabProject)
  getProjectById projectId = do
    projectWithoutTag <- getProjectWithoutTagById projectId
    projectTags <- getProjectTagsByProjectId projectId
    return $ translateGitlabProjectAndTag projectWithoutTag projectTags

  -- Return Project id that match the given name and serverId
  getProjectIdByNameAndServerId :: (String, Word64) -> IO (Word64)
  getProjectIdByNameAndServerId (name, serverId) = do
    conn <- connection
    sqlProjects <- query conn "select id from projects where name = ? and serverId = ?" (name, serverId)
    if (length sqlProjects) == 0
      then return 0
      else do
        projectsId <- forM sqlProjects $ \(Only id) -> return $ (id :: Word64)
        return (head projectsId)

  -- Return the tag matching the given id
  getProjectTagNameById :: Int -> IO ProjectTag
  getProjectTagNameById tagId = do
    conn <- connection
    sqlTags <- query conn "select name from tags where id = ?" (Only tagId)
    tag <- forM sqlTags $ \(Only name) -> return $ translateTagFromSql name
    return (head tag)

  --  Return all tags that match the given project id
  getProjectTagsByProjectId :: Int -> IO([ProjectTag])
  getProjectTagsByProjectId projectId = do
    conn <- connection
    sqlTags <- query conn "select tagId from project_tags where projectId = ?" (Only projectId)
    tags <- forM sqlTags $ \(Only tagId) -> getProjectTagNameById tagId
    return tags

  -- Return a project without tag mathcing the given id
  getProjectWithoutTagById :: Int -> IO (GitlabProjectWithoutTag)
  getProjectWithoutTagById projectId = do
    conn <- connection
    sqlProjects <- query conn "select name, description, ssh_url, http_url, stars from projects where id = ?" (Only projectId)
    projectsWithoutTag <- forM sqlProjects $ \(name, description, ssh_url, http_url, stars) -> return $ translateGitlabProjectWithoutTagFromSql name description ssh_url http_url (stars :: Int)
    return (head projectsWithoutTag)

  -- Return the id of the server matching the given url
  getServerIdByUrl :: GitlabUrl -> IO (Word64)
  getServerIdByUrl url = do
    conn <- connection
    sqlServers <- query conn "select id from servers where domain = ? and location = ? and port = ?" ((gitlabUrlDomain url), (gitlabUrlLocation url), (gitlabUrlPort url))
    if (length sqlServers) == 0
      then return 0
      else do
        serversId <- forM sqlServers $ \(Only id) -> return $ (id :: Word64)
        return (head serversId)

  -- Return the id of the tag matching the given name
  getTagIdByName :: String -> IO (Word64)
  getTagIdByName name = do
    conn <- connection
    sqlTags <- query conn "select id from tags where name = ?" (Only name)
    if (length sqlTags) == 0
      then return 0
      else do
        tagsId <- forM sqlTags $ \(Only id) -> return $ (id :: Word64)
        return (head tagsId)

  -- Return the user with the given email address from the database when possible
  getUserByEmail :: String -> IO [User]
  getUserByEmail email = do
    conn <- connection
    sqlUsers <- query conn "select email from users where email = ?" (Only email)
    haskellUsers <- forM sqlUsers $ \(Only email) -> return $ translateUserFromSql email
    return haskellUsers

  -- Return the user id if the given email and password are correct
  getUserByEmailAndPassword :: String -> String -> IO (Int)
  getUserByEmailAndPassword email password = do
    conn <- connection
    sqlSessionId <- query conn "select id from users where email = ? and password = ?" (email, password)
    haskellSessionId <- forM sqlSessionId $ \(Only id) -> return $ (id :: Int)
    if (length haskellSessionId > 0)
      then return (head haskellSessionId)
      else return (0 :: Int)

  -- Return the password of the user matching the given email
  getUserIdByEmail :: String -> IO (Int)
  getUserIdByEmail email = do
    conn <- connection
    sqlPasswords <- query conn "select id from users where email = ?" (Only email)
    if (length sqlPasswords) == 0
      then return (0 :: Int)
      else do
        ids <- forM sqlPasswords $ \(Only id) -> return $ (id :: Int)
        return (head ids)

  -- Return the password of the user matching the given email
  getUserPasswordByEmail :: String -> IO (String)
  getUserPasswordByEmail email = do
    conn <- connection
    sqlPasswords <- query conn "select password from users where email = ?" (Only email)
    if (length sqlPasswords) == 0
      then return ""
      else do
        passwords <- forM sqlPasswords $ \(Only password) -> return $ (password :: String)
        return (head passwords)

  -- Research the given string for all projects' descriptions and name
  getResearchResultExtended :: String -> IO ([GitlabProject])
  getResearchResultExtended research = do
    idMatchingNames <- searchSqlName research
    idMatchingDescription <- searchSqlDescription research
    let idMatching = idMatchingNames ++ idMatchingDescription
    matchingProjects <- getGitlabProjectByIds idMatching
    return matchingProjects

  -- Return all projects matching the given list of ids
  getGitlabProjectByIds :: [Int] -> IO ([GitlabProject])
  getGitlabProjectByIds ids = do
    projects <- mapM (\ x -> getProjectById x) ids
    return projects

  --  Return all tuples containing the given string in their second element
  matchingStringsIds :: [(Int, String)] -> String -> [(Int, String)]
  matchingStringsIds stringWithId substring = filter (\x -> matchingStrings x substring) stringWithId

  --  Return True if the given substring is part of the string in the given tuple, return False otherwise
  matchingStrings :: (Int, String) -> String -> Bool
  matchingStrings stringWithId substring
    | isInfixOf substring (snd stringWithId) = True
    | otherwise = False

  -- Search the given string for all projects' descriptions with an SQL query
  searchBTreeDescription :: String -> IO ([Int])
  searchBTreeDescription research = do
    conn <- connection
    let researchString = research++"%"
    sqlProjects <- query conn "select id from projects where description like ? " (Only researchString)
    if (length sqlProjects) == 0
      then return []
      else do
        projectsId <- forM sqlProjects $ \(Only id) -> return $ (id :: Int)
        return projectsId

  -- Search the given string for all projects' names with an SQL query
  searchBTreeName :: String -> IO ([Int])
  searchBTreeName research = do
    conn <- connection
    let researchString = research++"%"
    sqlProjects <- query conn "select id from projects where name like ? " (Only researchString)
    if (length sqlProjects) == 0
      then return []
      else do
        projectsId <- forM sqlProjects $ \(Only id) -> return $ (id :: Int)
        return projectsId

  -- Search the given string for all projects' descriptions by search inside a list of all projects
  searchHaskellDescription :: String -> IO ([Int])
  searchHaskellDescription research = do
    allProjectsDescriptions <- getAllProjectsDescriptions
    let researchedProjectsIdAndDescription = matchingStringsIds allProjectsDescriptions research
    return $ map (\x -> fst x) researchedProjectsIdAndDescription

  -- Search the given string for all projects' names by search inside a list of all projects
  searchHaskellName :: String -> IO ([Int])
  searchHaskellName research = do
    allProjectsNames <- getAllProjectsNames
    let researchedProjectsIdAndName = matchingStringsIds allProjectsNames research
    return $ map (\x -> fst x) researchedProjectsIdAndName

  -- Search the given string for all projects' descriptions with an SQL query
  searchSqlDescription :: String -> IO ([Int])
  searchSqlDescription research = do
    conn <- connection
    let researchString = "%"++research++"%"
    sqlProjects <- query conn "select id from projects where description like ? " (Only researchString)
    if (length sqlProjects) == 0
      then return []
      else do
        projectsId <- forM sqlProjects $ \(Only id) -> return $ (id :: Int)
        return projectsId

  -- Search the given string for all projects' names with an SQL query
  searchSqlName :: String -> IO ([Int])
  searchSqlName research = do
    conn <- connection
    let researchString = "%"++research++"%"
    sqlProjects <- query conn "select id from projects where name like ? " (Only researchString)
    if (length sqlProjects) == 0
      then return []
      else do
        projectsId <- forM sqlProjects $ \(Only id) -> return $ (id :: Int)
        return projectsId
