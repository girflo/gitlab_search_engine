{-# LANGUAGE OverloadedStrings #-}
module JSONParser where
  import Data.Aeson
  import Control.Applicative
  import Control.Monad (mzero)
  import qualified Data.ByteString.Lazy.Char8 as BS
  import Data.Text

  -- Create the type JsonProject that allow the translation from JSON to GitlabProject
  data JsonProject = JsonProject { getname            :: !Text
                     , getDescription     :: !Text
                     , getSshUrl          :: !Text
                     , getHttpUrl         :: !Text
                     , getStars           :: Int
                     } deriving (Show)

  -- Create a JsonProject with the information contain in the given JSON file
  instance FromJSON JsonProject where
    parseJSON (Object v) =
      JsonProject <$> v .: "name"
             <*> v .:  "description"
             <*> v .:  "ssh_url_to_repo"
             <*> v .:  "http_url_to_repo"
             <*> v .:  "star_count"
    parseJSON _ = mzero
