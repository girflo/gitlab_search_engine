import Control.Monad (forM_)
import Control.DeepSeq
import Criterion.Main
import Data.Tuple (swap)
import Data.Word
import Database.VCache
import System.Random
import Test.RandomStrings
import qualified Data.VCache.Trie as T
-- Local  modules
import CustomTypes
import DbSql
import Research
import Trie

instance NFData a => NFData (T.Trie a) where rnf = rnf . T.elems

-- Create the tries for the benchmark
setupEnv = do
  subDir <- vCacheInit
  tries <- createTries subDir
  return (tries)

-- Benchmarking function
benchmark = defaultMain [
   env setupEnv $ \ ~(tries) -> bgroup "tree" [
     bench "b-tree" $ nfIO $ searchBTree "ab",
     bench "trie"  $ nfIO $ searchTries "ab" tries
   ]
 ,  bgroup "SQL" [
     bench "sql" $ nfIO $ searchSql "ab",
     bench "haskell"  $ nfIO $ searchHaskell "ab"
   ]
 ]

main = do
  -- Generate 900 dummy project and isert them in the database
  populateWithDummyGitlabProjects 900
  -- Run the benchmark
  benchmark
  return ()

-- Create a list of dummy tags
createTagList :: [ProjectTag]
createTagList = [(ProjectTag "Name1"), (ProjectTag "Name2"), (ProjectTag "Name3"), (ProjectTag "Name4"), (ProjectTag "Name5")]

-- Insert a given dummy project in the database
insertDummyGitlabProject :: GitlabServer -> [ProjectTag] -> IO ()
insertDummyGitlabProject gitlabServer tags = do
  project <- generateDummyGitlabProjectWord tags
  serverId <- checkForAddingServer $ Just gitlabServer
  checkForAddingProject (project, serverId)

-- Generate a Project containing words
generateDummyGitlabProjectWord :: [ProjectTag] -> IO GitlabProject
generateDummyGitlabProjectWord tags = do
  projectName <- generateRandomProjectNameWord
  projectDescription <- generateRandomProjectDescriptionWord
  return $ GitlabProject projectName projectDescription "ssh_url" "http_url" 1 tags

-- Generate a Project containing ASCII char
generateDummyGitlabProjectAscii :: [ProjectTag] -> IO GitlabProject
generateDummyGitlabProjectAscii tags = do
  projectName <- generateRandomProjectNameAscii
  projectDescription <- generateRandomProjectDescriptionAscii
  return $ GitlabProject projectName projectDescription "ssh_url" "http_url" 1 tags

-- Generate a Project's name containing ASCII char
generateRandomProjectNameAscii :: IO String
generateRandomProjectNameAscii = do
  lengthName <- randomRIO (3, 10)
  word <- randomString randomASCII lengthName
  return word

-- Generate a Project's description containing ASCII char
generateRandomProjectDescriptionAscii :: IO String
generateRandomProjectDescriptionAscii = do
  wordsInDescription <- randomRIO (3, 100)
  descriptionWords <- randomStringsLen (randomString randomASCII) (10,30) wordsInDescription
  return $ unwords descriptionWords

-- Generate a Project's name containing words
generateRandomProjectNameWord :: IO String
generateRandomProjectNameWord = do
  lengthName <- randomRIO (3, 10)
  word <- randomWord randomASCII lengthName
  return word

-- Generate a Project's description containing words
generateRandomProjectDescriptionWord :: IO String
generateRandomProjectDescriptionWord = do
  wordsInDescription <- randomRIO (3, 100)
  descriptionWords <- randomStringsLen (randomWord randomASCII) (10,30) wordsInDescription
  return $ unwords descriptionWords

  -- Create x dummy projects into database with x the given Int
populateWithDummyGitlabProjects :: Int -> IO ()
populateWithDummyGitlabProjects iterations = do
  let tags = createTagList
  let server = GitlabServer (GitlabUrl "domain" "location" 80) "token"
  forM_ [1..iterations] $ \_ -> do
    insertDummyGitlabProject server tags
    return ()

-- Swap the two part of the given tuple
swapProjectNameAndId :: (Int, String) -> (String, Int)
swapProjectNameAndId project = swap project
